"""loading and checking nulls for ipl data set."""

import pandas as pd


data = pd.read_csv('~/data/matches.csv')
data.head()


data.info()

data.isnull().any()
